---
layout: post
title: Radio Towers
tagline: 1930s public radios around Kyoto City; the largely forgotten history of the early mass media audience in Japan.
category: Research
comments: false
excerpt_separator:  <!--more-->
---

![Radio tower in Funaoka Park, Kyoto](./imgs/rt.png){ width=50% }

<!--more-->

## ABSTRACT ##

This paper introduces Kyoto’s ‘radio towers’, a largely forgotten element of Japan’s early mass media history and a now scarce concrete reminder of the interpenetration of the private and state spheres characteristic of Japan in the 1930s and early 1940s. After a brief survey of the early development of radio broadcasting in Japan and the social, technological and political factors that fed into their conception, the paper presents a brief survey of the towers still known to be extant in Kyoto City. Attempts at preservation have been haphazard but there seems to be a growing realisation that these often overlooked and sometimes misconstrued pieces of public heritage are worthy of the attention of both academics seeking to understand the intersection of radio, state and audience in 1930s Japan and of those with an interest in preserving their local history. [@kb19]


[Download from Ritsumeikan Journal Website](http://www.ritsumei.ac.jp/ir/isaru/assets/file/journal/32-1_02Koga-Browes.pdf){ target="_blank" }

## Citation
