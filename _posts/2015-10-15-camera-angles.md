---
layout: post
title: "Camera Angles: Designed to Communicate?"
category: Research
comments: false
tagline: "Do current academic approaches to studying camera angles actually help us understand anything important about television news images?"
excerpt: "Book chapter: Camera angles as visual communication in news. The semiotic approach to visual communication assumes that images are created with a communicational intention. What if they are primarily the result of other types of decision-making processes?"
excerpt_separator:  <!--more-->
tags:
    - visual communication
    - semiotics
    - camerawork
    - television
---

![Chapter in this book!]({{ site.imgurl }}piazza-book-cover.jpg){ width=50% }

<!--more-->

## ABSTRACT ##

This chapter uses the ‘camera angle’ semiotic resource to investigate the difficulties inherent in the academic analysis of television news images. On one hand they are the products of media organisations, routinised commercial work and professional norms and understandings of the necessities of the presentation of visual narratives, on the other hand they can be seen as the raw material of ‘viewing’. To perhaps oversimplify, news images are a communicative interface between producers and viewers; however if the academic understanding and analysis of news images derives primarily from the standpoint of the viewer then we run the risk of misinterpreting the motivations and intentions of the image-maker. We assume that, when viewing news, the viewer is engaged in understanding a piece of communication about the world. I would argue that we cannot be certain that this is balanced by communicative intent on the part of the image-maker.

I review academic approaches to the understanding of ‘camera angles’ and investigate how these ideas align with samples of news images taken from Japan and the UK. I suggest that without an understanding of the imperatives of production many semiotically marked images will be misinterpreted as ‘meaningful’ where the intention behind their creation, in the particular form observed, is better understood as being primarily pragmatic [@KB:2015].

<!--Koga-Browes, S. P. (2015). Camera angles in television news: Designed to communicate? In Piazza, R., Haarman, L., and Caborn, A., editors, *Values and Choices in Television Discourse: A View from Both Sides of the Screen*. Palgrave Macmillan, London.-->

[Palgrave Book page](http://www.palgrave.com/jp/book/9781137478467)


## Reference ##
