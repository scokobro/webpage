---
layout: post
title: "Digital Watershed: Japanese TV's Digital Switchover"
category: Research
comments: false
excerpt: "Journal Article: The switch to digital broadcasting in Japan and what it might mean for the future of the industry."
tagline: "The background to Japan's 2011 switchover to full digital terrestrial broadcasting"
excerpt_separator:  <!--more-->
tags:
    - industry
    - television
    - Japan
    - broadcasting
---

![Discarded analog televisions]({{ site.imgurl }}tvs-widget.jpg){ width=60% }

<!--more-->

## ABSTRACT ##

The switch to digital terrestrial broadcasting on 24 July 2011 marked a watershed for the broadcasting industry in Japan. Digitalisation is the single largest industry-wide event since the advent of alternative distribution technologies, satellite and cable, in the 1980s. Preparation for the switch to digital, known as *chideji-ka*, has put existing business arrangements under pressure and has led to a renewed focus on the future shape of the industry. There is increasing acknowledgement that change, especially in the relationship between central and local broadcasters, is inevitable. This paper summarises the position of the industry at the beginning of its digital age, arguing for a new view of broadcasting in Japan that recognises the two-tier reality behind industry rhetoric. It also summarises the major options open to the industry as it looks to redefine itself in a much-changed media environment [@KB:2012].

[Journal website](http://www.tandfonline.com/doi/full/10.1080/10371397.2012.730482)

## Reference
