---
layout: post
title: "Social Distance: News in the UK and Japan"
category: Research
comments: false
excerpt: "Journal Article: Camera framing sizes and social relations in Japan and the UK"
excerpt_separator:  <!--more-->
tagline: "Framing of television news on the BBC and NHK, and the televisual construction of 'social distance'."
tags:
    - visual communication
    - semiotics
    - camerawork
    - television
    - framing
    - Japan
    - UK
---

![Typical 'piece-to-camera' size for the BBC!]({{ site.imgurl }}line-bbc-typical.jpg){ width=60% }

<!--more-->

## ABSTRACT ##

The potential of the camera framing, or shot-size, semiotic resource to encode meanings related to social distance has been recognized for some time. This study seeks to bring this resource into the remit of systematic analysis. Data are taken from screen measurements of portrayals of social actors in news programming produced by two national broadcasters, NHK in Japan and the BBC in the UK. Results for these two media outlets are compared and an attempt made to place the results in a meaningful cultural context. Analysis focuses on NHK’s images and the less familiar Japanese media system [@KB:2013].

[Journal website](http://vcj.sagepub.com/content/12/1/71.abstract)

---

![Linda Besners' 2013 piece on my paper](/imgs/hazlitt.png){ .limg-50}This paper was also used as the basis for an article in the online magazine 'Hazlitt'. You can read it [here](https://hazlitt.net/blog/what-extreme-close-ups-tell-us-about-japan).</br></br></br></br></br></br></br></br>

---

<!-- http://cdn.dropmark.com/1987/c95118ef1325ba3393650f9315e2984972c529c8/Screen%20Shot%202013-03-02%20at%2018.54.58.png -->


## Reference ##
