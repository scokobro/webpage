---
layout: post
title: Globalisation and Media
sidebar_link: true
comments: false
tagline: "(Graduate) Introducing study of the mass media for graduate students, with an emphasis on understanding the media's role in 'globalisation'."
tags:
    - media
    - television
    - newspapers
    - press
    - social media
    - globalisation
category: Courses
excerpt_separator:  <!--more-->
---

Information for the 'Globalisation and Media' is available at the link below.

[Globalisation and Media](https://www.notion.so/Globalisation-and-Media-2020-1-955bdd6b8f5f4b54b6361216b07f6f1b){ target=_blank} 
