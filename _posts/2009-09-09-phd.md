---
layout: post
title: Visual Semiotics of News Images in Japan and the UK
category: Research
comments: false
excerpt: "About my PhD from 2009, which was a study of the semiotic features of new TV images in Japan. It is based on an empirical survey of over 1500 news images which are coded and analysed using readings and categories suggested by 'social semiotics'."
tagline: "The visual semiotics of news in Japan; a survey of the sign-content of news images."
excerpt_separator:  <!--more-->
tags:
    - visual semiotics
    - japan
    - television
---

![A wide variety of framing sizes, not much standardisation here!](/imgs/skel-frame.jpg){ width=90% }

## ABSTRACT

Although application of theories of visual semiosis, meaning-making and meaning-sharing through pictures, is usually restricted to small-scale impressionistic studies of limited numbers of texts, its categories are also broadly applicable to larger corpora. This study applies a social semiotic approach to visual semiosis to a large body of mass media texts taken from news items, solely those dealing with domestic issues, broadcast on terrestrial analogue television in Japan during the summer of 2007.

Visual semiosis, the processes whereby images come to take on the form they have, is seen as a socially embedded activity. The contexts of this activity, formed of the ideological, social, cultural and physical surroundings in which it takes place, play a role in shaping the final visual message. This thesis attempts to trace some of the connections between image-forms used by news to illustrate social reality in Japan and their contexts of production. In doing so, it also tries to establish a sound basis on which visual semiotic categories can be used in analysis of televisual texts, it describes a replicable methodology by which consistent, objective descriptions of visual features of images can be obtained.

Part one of this study delineates approaches to visual semiosis, contrasting structuralist with 'social' semiotics, and, offers a short critique of visual semiotic categories. Part two looks at the contexts in which the televisual message is created. Firstly, the broader hierarchical social background to communication in Japan, focusing on *keigo* honorifics as markers of differences in 'social power', secondly, the ideological and practical contexts of television news production. Part three describes, in depth, the methodology used in deriving the study's data and offers descriptions of the visual semiotic resources used in television news in Japan and their inter-relationships with distributions of social power within Japanese society [@KB:2009]

Reference in [WorldCat](https://www.worldcat.org/title/reading-news-images-in-japan-visual-semiosis-in-the-context-of-television-representation-vol-1/oclc/661086671) Catalog

Reference in [British Library E-theses](http://ethos.bl.uk/OrderDetails.do?uin=uk.bl.ethos.521957) Repository

## Reference
