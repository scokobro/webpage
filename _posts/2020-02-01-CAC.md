---
layout: post
title: Cultural Awareness and Communication
sidebar_link: true
comments: false
category: Courses
tagline: "(Undergrad) Focussing on the concept of 'culture', the role of language, and shared human experiences of space and time."
tags:
    - culture
    - communication
    - language
excerpt_separator:  <!--more-->
---

Follow the link below for information on the Cultural Awareness and Communication course:

[Cultural Awareness and Communication](){ target=_blank }

<!--more-->

-----

# Readings #

These are some of the things we'll be reading, you will be able to download them from the relevant week's pages in the site linked to above.

Arnold, M. (1911)  *Culture & anarchy: an essay in political and social criticism*.  Macmillan and Company, London.

Bennett, M. J. (1998)  *Basic concepts of intercultural communication: Selected readings*.  Intercultural Press, Boston MA; London.

Brislin, R. W. and Kim, E. S. (2003)  Cultural diversity in people's understanding and uses of time.  *Applied Psychology*,  52(3):363–382

Brockman, M. J. (2015)  *This idea must die: scientific theories that are blocking progress*.  Harper Collins.

Brown, P. and Levinson, S. C. (1993)  'Uphill' and 'downhill' in Tzeltal.  *Journal of Linguistic Anthropology*,  3(1):46–74

Burgess, C. (2004)  Maintaining identities: Discourses of homogeneity in a rapidly globalizing Japan.  *electronic journal of contemporary japanese studies*

Dahl, S. (2004)  Intercultural Research: The Current State of Knowledge. SSRN eLibrary.

Knapp, M. L., Hall, J. A., and Horgan, T. G. (2013)  *Nonverbal communication in human interaction*. Cengage Learning.

Martí, F. (2005)  *Words and worlds: World languages review*. Multilingual Matters.

McSweeney, B. (2002)  Hofstede's model of national cultural differences and their consequences: A triumph of faith --- a failure of analysis.  *Human Relations*, 55(1):89

Pagel, M. (2012)  *Wired for culture: The natural history of human cooperation*. Penguin UK.

Piller, I. (2010)  *Intercultural communication: A Critical Introduction*. Edinburgh University Press, Edinburgh.

Sen, A. (2008)  *Identity and violence: The illusion of destiny*.  W.W. Norton \& Co, New York; London.

Spolsky, B. (1998)  *Sociolinguistics*. Oxford University Press, Oxford.
