---
layout: post
title: "Fake News Related Articles"
category: Random
excerpt: ""
tagline: "Random news articles on fake news"
comments: false
excerpt_separator:  <!--more-->
tags:
    - news
    - computers
    - technology
---

Below is an occasional collection of news articles dealing with the media and issues that might be of interest to IR/GS students. The actual articles may open is a new window or direct you to the original site, and there may be some dead links in there too ;)

<embed src="http://drp.mk/d0DrtVhNF" width=800px height=600px></embed>

---
