---
layout: page
title: About
sidebar_link: true
---

<!-- <p class="message">
  Hey there! This page is included as an example. Feel free to customize it
  for your own use upon downloading. Carry on!
</p> -->

![]({{ site.imgurl }}profile.gif){ .limg }This is the personal website of Scott Koga-Browes, Associate Professor at Ritsumeikan University's College of International Relations.


I built this site to bring together various aspects of the work I do at Ritsumeikan; pages in the 'Courses' section are aimed primarily at the students who take my courses and provide information and links to the material they will need during their studies.

My research work is reflected in the brief pages which detail my (meagre) research output. Not all of the material in the research section is entirely serious (please don't cite the daft bits!)

[ORCiD Profile ↗︎](https://orcid.org/0000-0002-9963-3830){ target=_blank}

## My CV ##

1986 - 1991: University of London, SOAS. BA Japanese\
1991 - 1993: TBS London Bureau\
1993 - 2002: Reuters<br>
&nbsp;&nbsp;'93 - '95: Duty News Organiser, GMTV<br>
&nbsp;&nbsp;'95 - '97: Seconded to NTV London Bureau<br>
&nbsp;&nbsp;'97 - '02: Reuters Financial TV, Tokyo<br>
2002 - 2005: Freelance production, UK & Spain\

2005 - 2006: Sheffield Hallam University. MA Media Studies\
2006 - 2009: Sheffield University. PhD, *Reading news images in Japan: Visual semiosis in the context of television representation.*\
2009 - 2011: Kyushu University. JSPS Post-doctoral Research Fellow.\
2012 - present: Ritsumeikan University, College of IR., Associate Professor\

---


<!-- https://scokobro.gitlab.io/rits/imgs/profile.png -->
