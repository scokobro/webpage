---
layout: category
title: Research
---

![]({{ site.imgurl }}panzani-widget.png){ width=30% .limg }My research looks at the television industry in Japan, in particular news production and the semiotics of news images. Links below will take you to pages for my published papers, as well as some other not-so-serious bits and pieces.


<!-- https://scokobro.gitlab.io/rits/imgs/panzani-widget.png -->
