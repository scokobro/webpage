---
layout: category
title: Courses
---

![]({{ site.imgurl }}sputnik-widget.jpg){ width=30% .limg }These are pages aimed at  students who are taking my undergraduate and graduate school course at Ritsumeikan University, Kyoto. Not all these courses are taught every semester so occasionally a page might be 'empty' ;)
