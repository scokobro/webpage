---
layout: page
title: Contact
sidebar_link: true
---

<p class="message">
[Applying for a Masters?](#masters)</br>
[Applying for a PhD?](#phd)</br>
</p>

## Mail Me ##

Email: <span class="fghj">pj&#x002E;ca&#x002E;i&#x0065;mustir&#x002E;cf&commat;bkps</span><style>
span.fghj{unicode-bidi: bidi-override;direction: rtl;}</style>

## Social Sites ##

[ResearchGate Profile ↗︎](https://www.researchgate.net/profile/Scott_Koga-Browes2){ target=_blank }

[Academia.edu Profile ↗︎](https://ritsumei.academia.edu/ScottKogaBrowes){ target=_blank }

[ORCiD Profile ↗︎](https://orcid.org/0000-0002-9963-3830){ target=_blank}

<!-- <div itemscope itemtype="https://schema.org/Person">ORCID Profile
<a itemprop="sameAs" content="https://orcid.org/0000-0002-9963-3830" href="https://orcid.org/0000-0002-9963-3830" target="orcid.widget" rel="me noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;float:left;" alt="ORCID iD icon"></a>
</div> -->
---

## Master's Applicants {#masters}
If you have completed an undergraduate degree in some field related to communications or mass media, and have an interest in Japan, then you should go ahead and contact me with details of the research project you would like to work on while at Ritsumeikan. However, depending on your area of interest and your particular academic background, your theoretical and practical strengths and weaknesses (we all have them!), a different supervisor may be more appropriate, and may be able to support you better.

### My strengths

- Good knowledge of mass media in Japan, especially the television industry, and journalism and news-related things.
- Fairly good knowledge of media theory, again especially that which deals with media production, news and visual communication.
- Fairly good writing and editing skills.
- Pretty mediocre programming and data acquisition/manipulation skills. Better than nothing, barely adequate for serious research.

### My weaknesses

- Limited knowledge of media in East Asia outside Japan; if you want to do research which will rely heavily on sources in Chinese or Korean, or  which requires a good understanding of politics or society in China or (either) Korea, someone else may be more helpful for you.
- 'Social media' cannot be studied in the same way, using the same theories and techniques, as old-style 'mass media' (tv, newspapers, radio etc). I am not really up to speed on the theory and techniques necessary to study 'social media' in a meaningful way. I am certainly not competent to teach the programming/scripting and data analysis skills you will need for much thorough 'social media' analysis.

### How to decide

It might help you choose an adviser if you think about what kind of knowledge/skills your research requires; for example, if you have decided that you want to look at how certain ideas are discussed on a social media channel in China, then in order for an adviser to have any useful input on such a project they would probably have to be able to understand the content of your raw research material, ie the comments/conversations you have extracted form the social media site. Thus, the ability to speak, or at least read, Chinese seems to be essential.

You should also consider what aspect of the 'discussion' you are interested in; is it the linguistic content, ie the words that people choose to use and the way they use language (would someone with a linguistics background be more useful to you?), or are you interested in other factors such as, *who* posts, and *when* do they post? Or perhaps it's the network 'friend' connections you are interested in, in which case someone with some network analysis skills might be helpful.

On the other hand maybe you are more interested in reproducing the methodology of a particular piece of research in a new field, or investigating the implications of a certain research technique; in this case knowledge of media theory, and research design and methodology - which are more widely applicable - are more important and it will be easier for you to come up with a project with which I can usefully assist you.

At Masters level you are not necessarily expected to carry out an entirely original project; your Masters degree should be time spent 'mastering' the basics of whatever field you have chosen, this requires you to get to know the fundamental literature and techniques, and often you will be able to use something - some other researchers' work (with appropriate credit of course) - as a model for your own.

### Practical Considerations

Finally, you have to choose a project which is 'manageable' given the resources you will have access to, for instance;

Time
~ You only have two years to complete your project, is this enough time for you to read everything you need to, complete any fieldwork, 'think', and write (rewrite, edit and check) a final thesis? If not, is it worth further restricting the scope of your question? Chose a shorter span of time for historical research, restrict a comparison to two countries instead of three, etc?

Money
~ Will your research require you to either travel (for interviews etc) or to purchase materials (DVDs, books etc)?

Available data
~ Any piece of research necessarily builds on and makes use of the work of previous researchers, as researchers we are also to some extent often reliant on data provided by governments or other bodies over which we have no control. Does the necessary data for your project exist? Do you have access to it? Remember, some types of commercial data are very hard if not impossible to come by, effectively being company secrets.

Available research
~ Is there sufficient previous research available for you project? If no-one has worked on the same (or at least 'similar') question you are interested in, will you be able to move forward on your own? You should also maybe think - if there doesn't seem to be any research on the question your are interested in - *why* no researcher has tried to answer the question you are proposing. Maybe it's 'unanswerable'!?!?


### Examples of previous students' projects

- The nature of satire in Japanese Media: The influence of political power and interest groups on satirical expression in Japanese media

-  International influences on the Production Process of the Japanese Animation Industry

- Representations of the family in Hong Kong television drama.

- Building Healthy Societies via Health Communication: Desirable Health Behaviours as Perceived in Television in Japan and Thailand


### Summary ###

If you have a background in media studies or communication and plan for your Master's thesis then please feel free to contact me to discuss whether I would be a suitable supervisor. If you do not have a plan or the necessary background then you will need to do some extra preparation before emailing me; read a couple of introductory books, have a look at some recent research that covers similar areas to that which you think you are interested in, think about a research plan of your own and so on. Then, *once you think you are in a position to seriously undertake Master's level study*, feel free to contact me.

------

## PhD Applicants {#phd}
Before you contact me, you need to have a fairly well-developed idea of what your research project will be, in many cases it will be an extension or elaboration of work you have carried out at Master's level, or will make extensive use of the theoretical material you got to know during your Masters degree.

You will need to be able to tell me...

1. **What is your research question?**
    - In rough terms, where did this question come from? Did it emerge from a particular reading or the work of a particular author? Or, does it come from a personal or professional experience?
    - Why is your question worth spending several years trying to answer?
    - Is it a 'good' question; one that cannot simply be answered 'yes' or 'no' and one that knowing the answer to which would help us understand the world, and/or contribute to academic study of the kind of problem it represents?
5. **How do you think you will be able to go about looking for answers to your question?**
    - What methods are you proposing to adopt in order to answer your question?
    - Why is this the most approach? What are the strengths and weaknesses of this approach compared to others?
2. **What do you need in terms of resources?**
    - Time: Can you propose a schedule for completion? If your research relies on things (not?) happening in the outside world, how might these affect your ability to stick to your plan?
    - Money: Do you have sufficient funds to make it through the period you intend to spend studying? Might you also need to travel, or purchases materials for your work? If so, have you identified potential sources of funding?
    - Information: Is there an extant body of knowledge and data on which you can build your work? Do you have access to the requisite data?
4. **How will you fulfil the degree requirements?**
    - Make sure you are aware of the requirements for fulfilling a PhD at Ritsumeikan University, these might be different from universities in other countries.
    - How do you plan to fulfil these requirements? If you need to publish papers as you progress, when and what do you plan to publish? Have you identified journals that you can submit your work to?

Looking forward to hearing from you!

---
